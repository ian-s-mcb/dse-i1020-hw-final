# DSE-i1020 Homework - Final Exam

Student written final for the class DSE-i1020 (Intro to Data Science.)

In the first notebook, I work with map data about after-school programs
in NYC and perform some basic aggregate statistics. In the second
notebook, I use a classifier on tabular data about the stylistic
features in national flags.

### Notes on env

Installing geopandas on Python 3.7 is a little tricky because one of
geopandas's dependencies (pyproj) needs to be installed from source, as
[reported here][1]. The steps below are adapted from [this
workaround][2].

```bash
mkvirtualenv hw-final --python=python3
pip install cython
pip install git+https://github.com/jswhit/pyproj.git#egg=pyproj
pip install geopandas descartes rtree
```

Besides geopandas, the rest of the dependencies for the two notebooks
are found in the `requirements.txt` file.

[1]: https://github.com/jswhit/pyproj/issues/136
[2]: https://github.com/geopandas/geopandas/issues/793#issue-347744943
