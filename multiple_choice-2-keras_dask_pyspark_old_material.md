# Multiple choices - 2 - Keras, Dask, PySpark, and old material

### Assignment directions
Write one question for each of the post-midterm exam topics:
    1. Deep Learning with Keras,
    1. Parallel Computing with Dask,
    1. Distributed Computing with PySpark.

Write two questions on old material, from before the midterm exam.

1. What is the name of the standard layer type in Keras where all the
nodes in the previous layer connect to all the nodes in the current
layer?
    * a) Sparse
    * b) Dense
    * c) Dropout
    * d) Sequential
1. When performing repeated operations on a dask dataframe that fits
into memory and the operations can potentially be parallelized, what can
be done to speed up the operations?
    * a) Use the delayed decorator on the operation
    * b) Use a dask bag instead of a dask dataframe
    * c) Use a persisted dask dataframe
    * d) Use a regular pandas dataframe
1. Which of the statements about Spark is FALSE?
    * a) It only handles numeric data
    * b) Its dataframes are immutable
    * c) It recommends that data is split in train and test test before applying transformations
    * d) For modeling, it requires all columns containing features to be combined into a single column
1. In time series analysis, pandas' shift function allows you to _?
    * ) Compare data at different points in time
    * ) Calculate the change between values at different points in time
    * ) Identify if your data is periodic
    * ) Switch between t-space and s-space domains
1. Which of the following CANNOT be automated in sklearn pipeline?
    * ) Replacing missing values
    * ) Normalizing (ie. scaling and centering)
    * ) Creating a model
    * ) Dealing with categorical values

Answers:
1. b)
1. c)
1. c)
1. a)
1. d)
