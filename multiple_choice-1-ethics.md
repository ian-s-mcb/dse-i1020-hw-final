# Multiple choices - 1 - Ethics

### Assignment directions

Write three mulitple choice questions based off the following two readings:
1. [ACM Code of Ethics and Professional](https://www.acm.org/code-of-ethics)
1. [Patreon’s Data Hack](https://bdes.datasociety.net/wp-content/uploads/2016/10/Patreon-Case-Study.pdf)


1. What makes online data different from other, more traditional datatypes used for decades by media scholars, such as newspapers and magazines?
    * a) Online data is more valuable and requires stronger protections against improper use
    * b) Online data isn't meant to be consumed by everybody even if it is public
    * c) Online data is allowed to be used for purposes not originally envisioned by the individuals providing the data
    * d) The field of online data research has professional norms and estabilished ethics code
1. What was NOT a reason in favor of using the hacked data?
    * a) Data is public, like a newspaper
    * b) We hope to serve the public good via our work
    * c) This is the data we want, but we can't get it via other means
    * d) Publishing the data can stop greater wrongdoing from occurring
1. What type of harm is NOT meant by the ACM General Ethics Principle: 'Avoid harm'?
    * a) Pressure to give up your own viewpoints
    * b) Damage to property
    * c) Physical or mental injury
    * d) Capricious or misguided reporting of risks

Answers
1. b)
2. d)
3. a)
